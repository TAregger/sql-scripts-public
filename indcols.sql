col table_name for a30
col column_name for a30
col column_position for 999

select i.TABLE_NAME, c.COLUMN_POSITION, c.COLUMN_NAME
  from dba_ind_columns c
  join dba_indexes i
    on (i.INDEX_NAME = c.INDEX_NAME)
 where i.INDEX_NAME = '&1'
 order by c.COLUMN_POSITION
/
