col high_value for a100
col column_name for a30
select TABLE_NAME,PARTITIONING_TYPE,SUBPARTITIONING_TYPE,PARTITION_COUNT from dba_part_tables 
where upper(table_name) LIKE 
                upper(CASE 
                    WHEN INSTR('&1','.') > 0 THEN 
                        SUBSTR('&1',INSTR('&1','.')+1)
                    ELSE
                        '&1'
                    END 
                     )   
AND owner LIKE
        CASE WHEN INSTR('&1','.') > 0 THEN
            UPPER(SUBSTR('&1',1,INSTR('&1','.')-1))
        ELSE
            user
        END 
/

select partition_name,high_value,SUBPARTITION_COUNT,interval from dba_tab_partitions 
where upper(table_name) LIKE 
                upper(CASE 
                    WHEN INSTR('&1','.') > 0 THEN 
                        SUBSTR('&1',INSTR('&1','.')+1)
                    ELSE
                        '&1'
                    END 
                     )   
AND table_owner LIKE
        CASE WHEN INSTR('&1','.') > 0 THEN
            UPPER(SUBSTR('&1',1,INSTR('&1','.')-1))
        ELSE
            user
        END 
order by partition_name
/

--select subpartition_name,high_value from dba_tab_subpartitions where partition_name = (select max(partition_name) from dba_tab_partitions where table_name=upper('&1'))  and table_name=upper('&1') order by 1;
select subpartition_name,high_value from dba_tab_subpartitions where (table_owner,table_name,partition_name) = (select table_owner,table_name,max(partition_name) from dba_tab_partitions
where upper(table_name) LIKE 
                upper(CASE 
                    WHEN INSTR('&1','.') > 0 THEN 
                        SUBSTR('&1',INSTR('&1','.')+1)
                    ELSE
                        '&1'
                    END 
                     )   
AND table_owner LIKE
        CASE WHEN INSTR('&1','.') > 0 THEN
            UPPER(SUBSTR('&1',1,INSTR('&1','.')-1))
        ELSE
            user
        END
group by table_owner,table_name)
order by 1
/

select COLUMN_POSITION,COLUMN_NAME from DBA_PART_KEY_COLUMNS
where upper(name) LIKE 
                upper(CASE 
                    WHEN INSTR('&1','.') > 0 THEN 
                        SUBSTR('&1',INSTR('&1','.')+1)
                    ELSE
                        '&1'
                    END 
                     )   
AND owner LIKE
        CASE WHEN INSTR('&1','.') > 0 THEN
            UPPER(SUBSTR('&1',1,INSTR('&1','.')-1))
        ELSE
            user
        END 
/

select COLUMN_POSITION,COLUMN_NAME from DBA_SUBPART_KEY_COLUMNS
where upper(name) LIKE 
                upper(CASE 
                    WHEN INSTR('&1','.') > 0 THEN 
                        SUBSTR('&1',INSTR('&1','.')+1)
                    ELSE
                        '&1'
                    END 
                     )   
AND owner LIKE
        CASE WHEN INSTR('&1','.') > 0 THEN
            UPPER(SUBSTR('&1',1,INSTR('&1','.')-1))
        ELSE
            user
        END 
/

--select TABLE_NAME,PARTITIONING_TYPE,SUBPARTITIONING_TYPE,PARTITION_COUNT from dba_part_tables where table_name=upper('&1');
--select partition_name,high_value,SUBPARTITION_COUNT from dba_tab_partitions where table_name=upper('&1');
--select subpartition_name,high_value from dba_tab_subpartitions where partition_name = (select max(partition_name) from dba_tab_partitions where table_name=upper('&1'))  and table_name=upper('&1') order by 1;
--select COLUMN_POSITION,COLUMN_NAME from DBA_PART_KEY_COLUMNS where name=upper('&1');
--select COLUMN_POSITION,COLUMN_NAME from DBA_SUBPART_KEY_COLUMNS where name=upper('&1');

