--------------------------------------------------------------------------------
-- Author:      Thomas Aregger
--------------------------------------------------------------------------------
col begin_interval_time for a21
col end_interval_time for a21
col statvalue for 999,999,999,999,999,999

select 
  to_char(sn.begin_interval_time, 'dd.mm.yyyy hh24:mi:ss') as begin_interval_time
 ,to_char(sn.end_interval_time, 'dd.mm.yyyy hh24:mi:ss') as end_interval_time
 ,st.stat_name, st.value as statvalue
from dba_hist_sysstat st
join dba_hist_snapshot sn on (sn.snap_id = st.snap_id)
where st.stat_name like '&1'
and sn.begin_interval_time
between to_date(to_char(sysdate, 'yyyy')||' &2', 'yyyy dd.mm hh24:mi') and to_date(to_char(sysdate, 'yyyy')||' &3', 'yyyy dd.mm hh24:mi')
order by sn.begin_interval_time,st.stat_name asc
/

