--
-- Purpose: Show all SQL Plan Baselines
-- Author: Thomas Aregger
--
col sql_handle for a20
col PLAN_NAME for a30
col creator for a15
col PARSING_SCHEMA_NAME for a15
col last_modified for a20
col last_executed for a20
col last_verified for a20
col enabled for a5
col accepted for a5
col fixed for a5
col reproduced for a5
col sql_text for a130 wrap
select
  sql_handle,
  plan_name,
  CREATOR,
  PARSING_SCHEMA_NAME,
  to_char(LAST_MODIFIED, 'yyyy-mm-dd hh24:mi:ss') as LAST_MODIFIED,
  to_char(LAST_EXECUTED, 'yyyy-mm-dd hh24:mi:ss') as LAST_EXECUTED,
--  to_char(LAST_VERIFIED, 'yyyy-mm-dd hh24:mi:ss') as LAST_VERIFIED,
  ENABLED,
  ACCEPTED,
  FIXED,
  REPRODUCED,
  substr(SQL_TEXT, 1, 130) as sql_text
from
dba_sql_plan_baselines;
