col mb for 9,999,999
select tablespace_name,status,round(sum(bytes)/1024/1024) as mb from dba_undo_extents group by tablespace_name,status order by 1,decode(status, 'EXPIRED', 1, 'UNEXPIRED', 2, 'ACTIVE', 3);
