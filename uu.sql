col u_username head USERNAME for a23
col u_sid head SID for a14 
col u_audsid head AUDSID for 9999999999
col u_osuser head OSUSER for a16
col u_machine head MACHINE for a40
col u_program head PROGRAM for a40
col u_module head MODULE for a40

select s.username u_username, ' ''' || s.sid || ',' || s.serial# || '''' u_sid, 
       s.audsid u_audsid,
       s.osuser u_osuser, 
       substr(s.machine,instr(s.machine,'\')) u_machine, 
       s.program u_program, 
       s.module u_module, 
       p.spid, 
       -- s.sql_address, 
       s.sql_hash_value, 
       s.last_call_et lastcall, 
       s.status 
       , s.logon_time
from 
    v$session s,
    v$process p
where
    s.paddr=p.addr
--and s.type!='BACKGROUND'
--and lower(s.username) like lower('&1')
and &1
--and s.status='ACTIVE'
/

