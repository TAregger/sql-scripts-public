-- Thomas Aregger 17.03.2014
prompt
prompt TM Locks: http://docs.oracle.com/cd/E11882_01/server.112/e25789/consist.htm#CNCPT1342
SELECT 
      chr(bitand(&1,-16777216)/16777215)||    
      chr(bitand(&1, 16711680)/65535)      Type, 
      mod(&1,16)                           lmode,
      decode(mod(&1,16), 1, 'Null', 2, 'SS: Sub share', 3, 'SX: Sub exclusive', 4, 'S: Share', 5, 'SSX: Share/sub exclusve', 6, 'X: Exclusive') lmode_text
from dual
/
