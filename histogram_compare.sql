declare
    type t_source is table of varchar2(4);
    type t_savtime is table of timestamp;
    type t_change is table of varchar2(10);

    v_source t_source;
    v_savtime t_savtime;
    v_change t_change;
begin
    --for rec in (select o.object_id, t.table_name,t.COLUMN_ID,t.column_name from dba_objects o join dba_tab_columns t on (o.object_name = t.table_name) where t.table_name='T_PASS_TRANSACTION') loop
    for rec in (select distinct (select o.object_id from dba_objects o where object_name=t.table_name and object_type='TABLE') as object_id, t.table_name,t.COLUMN_ID,t.column_name from dba_tab_columns t where t.table_name='T_PASS_TRANSACTION') loop
        select source,savtime,CASE WHEN lag(source,2) over (order by savtime) <> source and lag(savtime) over (order by savtime) <> savtime THEN 'CHANGE' ELSE '-' END as change  bulk collect into v_source, v_savtime, v_change from
        (SELECT 'COL' as source, savtime FROM SYS.WRI$_OPTSTAT_HISTHEAD_HISTORY WHERE obj#=rec.object_id and intcol#=rec.column_id
        union all
        SELECT distinct 'HIST' as source, savtime FROM SYS.WRI$_OPTSTAT_HISTGRM_HISTORY WHERE obj#=rec.object_id and intcol#=rec.column_id
        /*union all
        select distinct 'HIST' as source, sysdate from DBA_TAB_HISTOGRAMS where table_name=rec.table_name and COLUMN_NAME=rec.column_name*/
        order by savtime);
        for i in 1..v_source.COUNT loop
            dbms_output.put_line(rpad(rec.table_name||'('||rec.object_id||')',40,' ')||rpad(rec.column_name||'('||rec.column_id||')', 40, ' ')||rpad(v_source(i), 6, ' ')||v_savtime(i)||'      '||v_change(i));
        end loop;
        v_source.delete;
        v_savtime.delete;
        v_change.delete;
        dbms_output.put_line('-------------------------------------------------------------------');
        --exit;
    end loop;
end;
/
