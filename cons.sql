column cons_column_name heading COLUMN_NAME format a30
column column_name heading COLUMN_NAME format a30
column owner heading OWNER format a30
column table_name format a30
column constraint_name format a30
column r_constraint_name format a30
column constraint_type format a1

prompt Show constraints on table &1...

select
     co.owner,
     co.table_name,
     co.constraint_name,
     co.constraint_type,
     co.r_constraint_name,
     cc.column_name          cons_column_name,
     cc.position
from
     dba_constraints co,
     dba_cons_columns cc
where
    co.owner              = cc.owner
and co.table_name         = cc.table_name
and co.constraint_name    = cc.constraint_name
and upper(co.table_name) LIKE 
                upper(CASE 
                    WHEN INSTR('&1','.') > 0 THEN 
                        SUBSTR('&1',INSTR('&1','.')+1)
                    ELSE
                        '&1'
                    END
                     )
AND co.owner LIKE
        CASE WHEN INSTR('&1','.') > 0 THEN
            UPPER(SUBSTR('&1',1,INSTR('&1','.')-1))
        ELSE
            user
        END
order by
     owner,
     table_name,
     constraint_type,
     constraint_name,
     position,
     column_name
/

prompt Show REF constraints from table &1 (this can take a while) ...

SELECT CONSTRAINT_NAME, TABLE_NAME
  FROM DBA_CONSTRAINTS
 WHERE (OWNER, CONSTRAINT_NAME) IN
       (SELECT OWNER, R_CONSTRAINT_NAME
          FROM DBA_CONSTRAINTS
         WHERE R_CONSTRAINT_NAME IS NOT NULL
           AND UPPER(TABLE_NAME) LIKE UPPER(CASE
                                              WHEN INSTR('&1', '.') > 0 THEN
                                               SUBSTR('&1', INSTR('&1', '.') + 1)
                                              ELSE
                                               '&1'
                                            END)
           AND OWNER LIKE CASE
                 WHEN INSTR('&1', '.') > 0 THEN
                  UPPER(SUBSTR('&1', 1, INSTR('&1', '.') - 1))
                 ELSE
                  USER
               END)
/

prompt Show REF constraints to table &1 (this can take a while) ...

SELECT OWNER,
       TABLE_NAME,
       CONSTRAINT_NAME,
       c.constraint_type,
       C.R_CONSTRAINT_NAME,
       CC.COLUMN_NAME,
       CC.POSITION
  FROM DBA_CONSTRAINTS C
  JOIN DBA_CONS_COLUMNS CC
 USING (OWNER, TABLE_NAME, CONSTRAINT_NAME)
 WHERE OWNER LIKE CASE
         WHEN INSTR('&1', '.') > 0 THEN
          UPPER(SUBSTR('&1', 1, INSTR('&1', '.') - 1))
         ELSE
          USER
       END
   AND C.R_CONSTRAINT_NAME IN
       (SELECT CONSTRAINT_NAME
          FROM DBA_CONSTRAINTS
         WHERE OWNER LIKE CASE
                 WHEN INSTR('&1', '.') > 0 THEN
                  UPPER(SUBSTR('&1', 1, INSTR('&1', '.') - 1))
                 ELSE
                  USER
               END
           AND UPPER(TABLE_NAME) LIKE UPPER(CASE
                                              WHEN INSTR('&1', '.') > 0 THEN
                                               SUBSTR('&1', INSTR('&1', '.') + 1)
                                              ELSE
                                               '&1'
                                            END))
/
