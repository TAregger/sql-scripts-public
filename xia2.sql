prompt Display Execution plan in advanced format for sqlid &1

select * from table(dbms_xplan.display_cursor('&1', &2,'ADVANCED +ALLSTATS LAST +PEEKED_BINDS'));
