set serveroutput on

variable cnt number;
execute :cnt :=dbms_spm.load_plans_from_cursor_cache(sql_id=>'4va8m3u13kmxq');
select decode(:cnt, 1, 'OK', 'NOK') from dual;

declare
    cnt number;
    vHandle dba_sql_plan_baselines.sql_handle%type;
    vSQL    dba_sql_plan_baselines.sql_text%type;
    vPlan   dba_sql_plan_baselines.plan_name%type;
    vEnabled dba_sql_plan_baselines.enabled%type;
begin 
    SELECT sql_handle, sql_text, plan_name, enabled into vHandle, vSQL, vPlan, vEnabled FROM dba_sql_plan_baselines WHERE sql_text LIKE '%BaselineDemo-1%';
    dbms_output.put_line('Handle -> ' || vHandle);
    dbms_output.put_line('Plan -> ' || vHandle);
    dbms_output.put_line('Enabled -> ' || vEnabled);

    dbms_output.put_line('---------------------------------------------');

    :cnt := DBMS_SPM.ALTER_SQL_PLAN_BASELINE (
                  SQL_HANDLE     => vHandle, 
                  PLAN_NAME      => vPlan, 
                  ATTRIBUTE_NAME =>'enabled',
                  ATTRIBUTE_VALUE=>'NO');

    dbms_output.put_line(:cnt);

    SELECT sql_handle, sql_text, plan_name, enabled into vHandle, vSQL, vPlan, vEnabled FROM dba_sql_plan_baselines WHERE sql_text LIKE '%BaselineDemo-1%';
    dbms_output.put_line('Handle -> ' || vHandle);
    dbms_output.put_line('Plan -> ' || vHandle);
    dbms_output.put_line('Enabled -> ' || vEnabled);

    dbms_output.put_line('---------------------------------------------');

    :cnt:=dbms_spm.load_plans_from_cursor_cache(
                       sql_id          =>'6qqcwaywbu43t', 
                       plan_hash_value => 3900446664, 
                       sql_handle      => vHandle);

end;
/

SELECT sql_handle, sql_text, plan_name, enabled FROM dba_sql_plan_baselines WHERE sql_text LIKE '%BaselineDemo-1%';

prompt "--PAUSE--"
pause

declare
    cnt number;
    vHandle dba_sql_plan_baselines.sql_handle%type;
begin 
    SELECT distinct sql_handle into vHandle FROM dba_sql_plan_baselines WHERE sql_text LIKE '%BaselineDemo-1%';

    :cnt := DBMS_SPM.DROP_SQL_PLAN_BASELINE (
               sql_handle => vHandle, 
               plan_name  => NULL);

end;
/
