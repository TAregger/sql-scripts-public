drop table t1;
create table t1 (c1 number(6), c2 varchar2(2000));

begin
    for i in 1..10 loop
        insert into t1 values(i, lpad('#', 1000, '#'));
    end loop;
    commit;
end;
/

begin
    for i in 1..100000 loop
        insert into t1 values(999999, lpad('#', 1000, '#'));
    end loop;
    commit;
end;
/

--create index i1 on t1 (c1, c2);
create index i1 on t1 (c1);

exec dbms_stats.gather_table_stats(user, 'T1');
