--------------------------------------------------------------------------------
-- Author:      Thomas Aregger
--------------------------------------------------------------------------------
col begin_interval_time for a21
col end_interval_time for a21
col total_waits for 999,999,999,999,999,999
col delta_waits for 999,999,999,999,999,999
col time_waited_micro for 999,999,999,999,999,999
col delta_time_waited_micro for 999,999,999,999,999,999
col TIME_WAITED_PER_WAIT for 999,999,999,999
col DELTA_TIME_WAITED_PER_WAIT for 999,999,999,999

select 
  to_char(sn.begin_interval_time, 'dd.mm.yyyy hh24:mi:ss') as begin_interval_time
 ,to_char(sn.end_interval_time, 'dd.mm.yyyy hh24:mi:ss') as end_interval_time
 ,st.event_name
 ,(st.time_waited_micro - lag(st.time_waited_micro) over (order by sn.begin_interval_time)) as delta_time_waited_micro
 ,(st.total_waits - lag(st.total_waits) over (order by sn.begin_interval_time)) as delta_waits
 ,(st.time_waited_micro - lag(st.time_waited_micro) over (order by sn.begin_interval_time))
  /(st.total_waits - lag(st.total_waits) over (order by sn.begin_interval_time)) as delta_time_waited_per_wait
 ,st.total_waits
 ,st.time_waited_micro
 ,st.time_waited_micro/st.total_waits as TIME_WAITED_PER_WAIT
from dba_hist_system_event st
join dba_hist_snapshot sn on (sn.snap_id = st.snap_id)
where st.event_name like '&1'
and sn.begin_interval_time
between to_date(to_char(sysdate, 'yyyy')||' &2', 'yyyy dd.mm hh24:mi') and to_date(to_char(sysdate, 'yyyy')||' &3', 'yyyy dd.mm hh24:mi')
order by sn.begin_interval_time,st.event_name asc
/

