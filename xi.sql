prompt eXplain the execution plan for sqlid &1 child &2....

select plan_table_output from (
select
  plan_table_output,
  case
    when regexp_like(plan_table_output, 'SQL_ID.*|HASH_VALUE.*|EXPLAINED SQL STATEMENT.*') then 1 -- show rows starting with SQL_ID
    when regexp_like(lag(plan_table_output) over (order by rownum), 'SQL_ID.*|HASH_VALUE.*|EXPLAINED SQL STATEMENT.*') then 1 -- show the row after SQL_ID too 
    when (lead(plan_table_output) over (order by rownum)) like '| Id%' then 1 -- show the row before the plan starts (the top line of the ascii plan table)
    when plan_table_output like 'Plan hash value%' then 1 -- show the plan hash value row
    else last_value(
      case
        when plan_table_output like '| Id%' then 1
        when regexp_like(plan_table_output, 'SQL_ID.*|HASH_VALUE.*|EXPLAINED SQL STATEMENT.*') then 0 
	else null
    end) ignore nulls over (order by rownum) -- show rows coming after the plan table header. Don't show rows coming after the SQL_ID/HASH_VALUE string (this is where the SQL statement text is)
  end as show
FROM table(dbms_xplan.display_cursor('&1',CASE WHEN '&2' = '%' THEN null ELSE '&2' END,'ALLSTATS LAST +PEEKED_BINDS +PARTITION')))
--where show = 1
/
