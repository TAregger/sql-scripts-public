prompt Display Execution plan in advanced format for sqlid &1

select plan_table_output from (
select
  plan_table_output,
  case
    when plan_table_output like 'SQL_ID%' then 1
    when (lag(plan_table_output) over (order by rownum)) like 'SQL_ID%' then 1
    when (lead(plan_table_output) over (order by rownum)) like '| Id%' then 2
    when plan_table_output like 'Plan hash value%' then 2
    else null
  end as ind,
  last_value(
    case
       when plan_table_output like 'SQL_ID%' then 1
       when plan_table_output like '| Id%' then 2
       else null
    end) ignore nulls over (order by rownum) as ind2
FROM table(dbms_xplan.display_awr('&1',null,null,'ALL')))
where not (ind is null and ind2 = 1)
/

