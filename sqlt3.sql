column sqlt_sql_text	heading SQL_TEXT  for a250 word_wrap

with sqlt as (
select 
	sql_fulltext sqlt_sql_text
from 
	v$sql 
where 
	sql_id = '&1'
and rownum = 1)
select dbms_lob.substr(sqlt_sql_text, 4000, 1) sqlt_sql_text from sqlt
union
select dbms_lob.substr(sqlt_sql_text, 4000, 4001) sqlt_sql_text from sqlt
union
select dbms_lob.substr(sqlt_sql_text, 4000, 8001) sqlt_sql_text from sqlt
union
select dbms_lob.substr(sqlt_sql_text, 4000, 12001) sqlt_sql_text from sqlt
union
select dbms_lob.substr(sqlt_sql_text, 4000, 16001) sqlt_sql_text from sqlt
union
select dbms_lob.substr(sqlt_sql_text, 4000, 20001) sqlt_sql_text from sqlt
union
select dbms_lob.substr(sqlt_sql_text, 4000, 24001) sqlt_sql_text from sqlt
union
select dbms_lob.substr(sqlt_sql_text, 4000, 28001) sqlt_sql_text from sqlt
union
select dbms_lob.substr(sqlt_sql_text, 4000, 32001) sqlt_sql_text from sqlt
union
select dbms_lob.substr(sqlt_sql_text, 4000, 36001) sqlt_sql_text from sqlt
/

