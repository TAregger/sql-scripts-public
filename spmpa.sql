--
-- Purpose: Show execution plan of SQL Plan Baseline
-- Author: Thomas Aregger
--
select * from table(DBMS_XPLAN.DISPLAY_SQL_PLAN_BASELINE('&1', decode('&2', '%', null, '&2'), 'ADVANCED'));
