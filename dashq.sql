--------------------------------------------------------------------------------
-- Author:      Thomas Aregger
--              
-- Usage:       
--     @ashq <grouping_cols> <filters> <fromtime> <totime>
--
-- Example:
--     @ashq username,sql_id session_type='FOREGROUND' sysdate-1/24 sysdate
--
-- Other:
--     This script uses only the in-memory V$ACTIVE_SESSION_HISTORY
--              
--------------------------------------------------------------------------------
COL totalseconds HEAD "Total|Seconds" FOR 99999999
COL pct HEAD "Pct%" FOR a7
break on report
compute sum of totalseconds on report

set heading off timing off
select 'From: '||to_date(to_char(sysdate, 'yyyy')||' &3', 'yyyy dd.mm hh24:mi') from dual
union
select 'To:   '||to_date(to_char(sysdate, 'yyyy')||' &4', 'yyyy dd.mm hh24:mi') from dual
/
set heading on timing on


select count(*)*10 totalseconds,
--select count(distinct sample_time)*10 totalseconds,
    LPAD(ROUND(RATIO_TO_REPORT(COUNT(*)) OVER () * 100)||'%',5,' ') pct,
    &1
from dba_hist_active_sess_history
where &2
and sample_time
between to_date(to_char(sysdate, 'yyyy')||' &3', 'yyyy dd.mm hh24:mi') and to_date(to_char(sysdate, 'yyyy')||' &4', 'yyyy dd.mm hh24:mi')
--and dbid=2287658810
group by &1
order by 1 desc,&1
--order by &1
/

