select begin_time,end_time,round(sum(SMALL_READ_MBPS), 1) small_reads_mbps, round(sum(SMALL_WRITE_MBPS),1) small_writes_mbps, round(sum(LARGE_READ_MBPS),1) large_reads_mbps, round(sum(LARGE_WRITE_MBPS),1) large_writes_mbps
from  V$IOFUNCMETRIC_HISTORY
group by begin_time,end_time
order by begin_time asc;
