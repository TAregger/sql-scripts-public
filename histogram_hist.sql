select
  savtime,
  ENDPOINT,
  bucket,
  bucket-lag(bucket) over (partition by savtime order by savtime,bucket) as cnt
from WRI$_OPTSTAT_HISTGRM_HISTORY h
join dba_objects o on (o.object_id = h.obj#)
join dba_tab_cols c on (o.object_name = c.table_name and c.column_id = h.intcol#)
where o.object_name = 'T_PASS_TRANSACTION'
and o.object_type = 'TABLE'
and c.column_name = 'ACQUIRINGSTATE'
order by savtime,bucket
/
