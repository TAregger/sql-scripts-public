--
-- Purpose: Drop SQL Plan Baseline
-- Author: Thomas Aregger
--
var baselines_dropped number;
exec :baselines_dropped := DBMS_SPM.DROP_SQL_PLAN_BASELINE(sql_handle => '&1', plan_name => '&2');

prompt
print :baselines_dropped
