-- col_stats
-- Martin Widlake mdw 21/03/2003
-- MDW 11/12/09 enhanced to include more translations of low_value/high_value
-- pilfered from Gary Myers blog
col owner        form a6 word wrap
col table_name   form a15 word wrap
col column_name  form a30 word wrap
col data_type    form a12
col N            form a1
col H            form a1
col num_vals     form 999,999,999,999,999
col dnsty        form 0.9999
col num_nulls    form 999,999,999,999,999
col low_value    form a50 word wrap
col hi_value     form a50 word wrap
col data_type    form a10
col partition_name for a22
set lines 340
break on owner nodup on table_name nodup
break on partition_name
select --owner
--      ,table_name
       p.partition_name
      ,column_name
      ,t.data_type
      ,t.nullable N
      ,substr(p.histogram,0,1) H
      ,p.num_distinct num_vals
      ,p.num_nulls
      ,p.density dnsty
,decode(data_type
  ,'NUMBER'       ,to_char(utl_raw.cast_to_number(p.low_value))
  ,'VARCHAR2'     ,to_char(utl_raw.cast_to_varchar2(p.low_value))
  ,'NVARCHAR2'    ,to_char(utl_raw.cast_to_nvarchar2(p.low_value))
  ,'BINARY_DOUBLE',to_char(utl_raw.cast_to_binary_double(p.low_value))
  ,'BINARY_FLOAT' ,to_char(utl_raw.cast_to_binary_float(p.low_value))
  ,'DATE',to_char(1780+to_number(substr(p.low_value,1,2),'XX')
         +to_number(substr(p.low_value,3,2),'XX'))||'-'
       ||to_number(substr(p.low_value,5,2),'XX')||'-'
       ||to_number(substr(p.low_value,7,2),'XX')||' '
       ||(to_number(substr(p.low_value,9,2),'XX')-1)||':'
       ||(to_number(substr(p.low_value,11,2),'XX')-1)||':'
       ||(to_number(substr(p.low_value,13,2),'XX')-1)
,  p.low_value
       ) low_value
,decode(data_type
  ,'NUMBER'       ,to_char(utl_raw.cast_to_number(p.high_value))
  ,'VARCHAR2'     ,to_char(utl_raw.cast_to_varchar2(p.high_value))
  ,'NVARCHAR2'    ,to_char(utl_raw.cast_to_nvarchar2(p.high_value))
  ,'BINARY_DOUBLE',to_char(utl_raw.cast_to_binary_double(p.high_value))
  ,'BINARY_FLOAT' ,to_char(utl_raw.cast_to_binary_float(p.high_value))
  ,'DATE',to_char(1780+to_number(substr(p.high_value,1,2),'XX')
         +to_number(substr(p.high_value,3,2),'XX'))||'-'
       ||to_number(substr(p.high_value,5,2),'XX')||'-'
       ||to_number(substr(p.high_value,7,2),'XX')||' '
       ||(to_number(substr(p.high_value,9,2),'XX')-1)||':'
       ||(to_number(substr(p.high_value,11,2),'XX')-1)||':'
       ||(to_number(substr(p.high_value,13,2),'XX')-1)
,  p.high_value
       ) hi_value
from dba_tab_columns t
join dba_part_col_statistics p
using (owner,table_name,column_name)
where owner      = upper('&tab_own')
and   table_name = upper('&tab_name')
ORDER BY partition_name,COLUMN_ID
/
clear colu
clear breaks
