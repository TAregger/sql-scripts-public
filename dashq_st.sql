--------------------------------------------------------------------------------
-- Author:      Thomas Aregger
--              
-- Usage:       
--     @dashq_st <filters> <fromtime> <totime>
--
-- Example:
--     @dashq_st sql_id='fyd108zrws8n4' '02.10 16:43' '03.10 17:00'
--
-- Other:
--     This script uses only the in-memory V$ACTIVE_SESSION_HISTORY
--              
--------------------------------------------------------------------------------

set heading off timing off
select 'From: '||to_date(to_char(sysdate, 'yyyy')||' &2', 'yyyy dd.mm hh24:mi') from dual
union
select 'To:   '||to_date(to_char(sysdate, 'yyyy')||' &3', 'yyyy dd.mm hh24:mi') from dual
/
set heading on timing on


select min(sample_time), max(sample_time)
from dba_hist_active_sess_history
where &1
and sample_time
between to_date(to_char(sysdate, 'yyyy')||' &2', 'yyyy dd.mm hh24:mi') and to_date(to_char(sysdate, 'yyyy')||' &3', 'yyyy dd.mm hh24:mi')
--and dbid=2287658810
/

