def _x_temp_env=&_tpt_tempdir/env_&_tpt_tempfile..sql
def _x_temp_sql=&_tpt_tempdir/sql_&_tpt_tempfile..sql

set termout off
store set &_x_temp_env replace
save      &_x_temp_sql replace
set termout on


set pages 5000 lines 180
set heading on
set feedback off
set serveroutput on

col _dg_name for a15 heading "DG Name"
col _dg_totalmb for a20 heading "Total"
col _dg_totalfree for a20 heading "Total Free"
col _dg_totalused for a20 heading "Total Used"
col _dg_freepct for a6 heading "Free %"
col _dg_state for a15 heading "DG State"
col _dg_numfiles for 99999 heading "DG State"


exec dbms_output.put_line('---');
exec dbms_output.put_line('--- DISKGROUPS OVERVIEW');
exec dbms_output.put_line('---');

SELECT
        NAME as "_dg_name",
        STATE as "_dg_state",
        lpad(TOTAL_MB/1024,6)||' GB' as "_dg_totalmb",
        lpad(FREE_MB/1024,6)||' GB' as "_dg_totalfree",
        lpad(FREE_MB/TOTAL_MB*100,4)||'%' as "_dg_freepct"
FROM
        v$asm_diskgroup;

exec dbms_output.put_line('---');
exec dbms_output.put_line('--- RECOVERYAREA OVERVIEW');
exec dbms_output.put_line('---');

SELECT
        NAME as "_dg_name",
        lpad(SPACE_LIMIT/1024/1024/1024,6)||' GB' as "_dg_totalmb",
        lpad((SPACE_LIMIT-SPACE_USED)/1024/1024/1024,6)||' GB' as "_dg_totalfree",
        lpad(SPACE_USED/1024/1024/1024,6)||' GB' as "_dg_totalused",
        NUMBER_OF_FILES as "_dg_numfiles"
FROM
        v$recovery_file_dest;

exec dbms_output.put_line('---');
set feedback on

set termout off
@/&_x_temp_env
get &_x_temp_sql
set termout on

