--------------------------------------------------------------------------------
-- Author:      Thomas Aregger
--------------------------------------------------------------------------------
col begin_interval_time for a21
col day noprint
col PLAN_HASH_VALUE for 9999999999
col BUFFER_GETS_DELTA for 999,999,999,999
col EXECUTIONS_DELTA for 999,999,999,999
col ELAPSED_TIME_DELTA for 999,999,999,999
col DISK_READS_DELTA for 999,999,999,999
col CPU_TIME_DELTA for 999,999,999,999
col PARSE_CALLS_DELTA for 999,999,999,999
col ELAPSED_TIME_DELTA heading "Elapsed Time (microseconds)" 
col GETS_PER_EXEC for 999,999,999,999.9
col ELA_PER_EXEC heading "Ela per exec (microseconds)" for 99999999999
col DR_PER_EXEC for 999,999,999,999.9
col ROWS_PROCESSED_DELTA for 999,999,999,999
col ROWS_PER_EXEC for 999,999,999,999

break on day skip 1;
compute sum label 'Total' of EXECUTIONS_DELTA on day
compute sum label 'Total' of BUFFER_GETS_DELTA on day
compute sum label 'Total' of ELAPSED_TIME_DELTA on day
compute sum label 'Total' of CPU_TIME_DELTA on day
compute sum label 'Total' of BUFFER_GETS_DELTA on day
compute sum label 'Total' of PARSE_CALLS_DELTA on day
compute sum label 'Total' of DISK_READS_DELTA on day
compute sum label 'Total' of ROWS_PROCESSED_DELTA on day

select trunc(begin_interval_time) as day, to_char(sn.begin_interval_time, 'dd.mm.yyyy hh24:mi:ss') as begin_interval_time, st.plan_hash_value,
       st.EXECUTIONS_DELTA
      ,st.BUFFER_GETS_DELTA
      ,st.BUFFER_GETS_DELTA/decode(st.EXECUTIONS_DELTA, 0, 1, st.EXECUTIONS_DELTA) as GETS_PER_EXEC
      ,st.ELAPSED_TIME_DELTA
      ,st.ELAPSED_TIME_DELTA/decode(st.EXECUTIONS_DELTA, 0, 1, st.EXECUTIONS_DELTA) as ELA_PER_EXEC
      ,st.DISK_READS_DELTA
      ,st.DISK_READS_DELTA/decode(st.EXECUTIONS_DELTA, 0, 1, st.EXECUTIONS_DELTA) as DR_PER_ELA
      ,st.CPU_TIME_DELTA
      ,st.PARSE_CALLS_DELTA
      ,st.ROWS_PROCESSED_DELTA
      ,st.ROWS_PROCESSED_DELTA/decode(st.EXECUTIONS_DELTA, 0, 1, st.EXECUTIONS_DELTA) as ROWS_PER_EXEC
      ,st.PLSEXEC_TIME_DELTA
      --,st.*
from dba_hist_sqlstat st
join dba_hist_snapshot sn on (sn.snap_id = st.snap_id)
where sql_id='&1'
and sn.begin_interval_time
between to_date(to_char(sysdate, 'yyyy')||' &2', 'yyyy dd.mm hh24:mi') and to_date(to_char(sysdate, 'yyyy')||' &3', 'yyyy dd.mm hh24:mi')
--between to_date('&2', 'yyyy dd.mm hh24:mi') and to_date('&3', 'yyyy dd.mm hh24:mi')
order by sn.begin_interval_time asc
/

