col p_name head NAME for a40
col p_value head VALUE for a40
col p_descr head DESCRIPTION for a80

select n.ksppinm p_name, c.ksppstvl p_value, n.KSPPDESC p_descr
from sys.x$ksppi n, sys.x$ksppcv c
where n.indx=c.indx
--and lower(n.ksppinm) like lower('%&1%')
and (lower(n.ksppinm) like lower('%&1%') or lower(n.KSPPDESC) like lower('%&1%'))
/
