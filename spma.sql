--
-- Purpose: Alter SQL Plan Baseline
-- Author: Thomas Aregger
--
var baselines_altered number;
exec :baselines_altered := DBMS_SPM.ALTER_SQL_PLAN_BASELINE(sql_handle => '&1', plan_name => '&2', attribute_name => '&3', attribute_value => '&4');

prompt
print :baselines_altered 
