--------------------------------------------------------------------------------
--
-- File name:   dash_sql_runtimes.sql
-- Purpose:     Shows start and end times of query executions from AWR data
--              
-- Author:      Thomas Aregger
-- Date:        19.03.2014
--              
-- Usage:       @dash_sql_runtimes.sql <sql_id> <start_time> <end_time>
--              Time Format 'dd.mm hh24:mi' or '-' for no restriction
--
--------------------------------------------------------------------------------


col start_time for a20
col end_time for a20
col runtime for a15
col sql_plan_hash_value for a20
col child_number for 9999
select * from (
select to_number(to_char(sample_time, 'DDD')||sql_exec_id) as ddd_sql_exec_id,
--if plan id is different than the one in the row before, mark it with an asterisk
lpad(sql_plan_hash_value||case when (lag(sql_plan_hash_value) over (order by to_number(to_char(sample_time, 'DDD')||sql_exec_id))) <> sql_plan_hash_value THEN '*' else ' ' end, 20, ' ') as sql_plan_hash_value,
sql_child_number as child_number,
to_char(min(sample_time), 'dd.mm hh24:mi:ss') start_time,
to_char(max(sample_time), 'dd.mm hh24:mi:ss') end_time,
regexp_replace(to_char(max(sample_time)-min(sample_time)), '\+\d{9} (\d{2}:\d{2}:\d{2}).\d{3}', '\1') as runtime
from dba_hist_active_sess_history
where sql_id='&1'
group by to_number(to_char(sample_time, 'DDD')||sql_exec_id), sql_plan_hash_value, sql_child_number
--order by to_date(start_time, 'dd.mm hh24:mi:ss')
) where to_date(start_time, 'dd.mm hh24:mi:ss') <= decode('&3', '-', sysdate, to_date('&3', 'dd.mm hh24:mi'))
and to_date(start_time, 'dd.mm hh24:mi:ss') >= decode('&2', '-', (select min(sample_time) from dba_hist_active_sess_history), to_date('&2', 'dd.mm hh24:mi'))
order by start_time,ddd_sql_exec_id
/
