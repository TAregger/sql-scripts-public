
variable N1 NUMBER
variable N2 VARCHAR2(128)
variable N3 VARCHAR2(128)

begin

:N1 := 100;
:N2 := '201403150000000';
:N3 := '201403159999999';

end;

/

select /* test cd9bq99pckqxd */ /*+ gather_plan_statistics */
/*T;;com.telekurs.pass.batch.donedate.common.dao.DoneDateGroupsWithSEDAO.GROUPS_WITH_SE_SQL*/  a.job, MIN(a.SETTLINGREFNO) AS
minSettlingRefNo, MAX(a.SETTLINGREFNO) AS maxSettlingRefNo FROM ( SELECT /*+ parallel(s, 24) */ s.SETTLINGREFNO, TRUNC((ROW_NUMBER()
OVER(ORDER BY s.SETTLINGREFNO) - 1) / :N1 ) AS job               FROM T_SETTLING s               WHERE s.DONEDATE is null                AND
s.SETTLINGREFNO between :N2  /*minsettlingrefno*/               AND :N3  /*maxsettlingrefno*/                AND NOT EXISTS
(SELECT /*+ parallel(se, 24) */ 1                        FROM T_SETTLEMENT_ENTRY se                       JOIN T_CONSOLIDATION_GROUP cg  on
cg.GROUPID                        = se.GROUPID                       JOIN  T_CONSOLIDATION_SUBGROUP csu                        on
csu.GROUPID                       =  cg.GROUPID                       WHERE se.SETTLINGREFNO                       = s.SETTLINGREFNO
AND csu.SUBGROUPSTATE                       = 1) ) a GROUP BY a.job ORDER BY a.job;

