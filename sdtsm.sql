col "MaxSize MB" for 999,999,999
col "Alloz MB" for 999,999,999
col "Belegt MB" for 999,999,999
col "Bel_MS %" for 999
col "Bel_AL %" for 999
col "Type" for a15
col "Status" for a15
col "Bigfile" for a3
col TS for a20


SELECT
d.tablespace_name TS,
d.status "Status",
d.contents "Type",
d.bigfile "Bigfile",
--d.extent_management "Extent Mgmt",
ROUND(NVL(a.MAXBYTES1 , 0), 3)/1024/1024 "MaxSize MB",
ROUND(NVL(a.bytes , 0), 3)/1024/1024 "Alloz MB",
ROUND(NVL(a.bytes - NVL(f.bytes, 0), 0), 3)/1024/1024 "Belegt MB",
ROUND(NVL((a.bytes - NVL(f.bytes, 0)) / a.MAXBYTES1 * 100, 0), 2) "Bel_MS %",
ROUND(NVL((a.bytes - NVL(f.bytes, 0)) / a.bytes * 100, 0), 2) "Bel_AL %"
FROM
sys.dba_tablespaces d,
(
SELECT
tablespace_name,
sum(bytes) bytes,
sum(decode(autoextensible,'YES',maxbytes,bytes)) MAXBYTES1
FROM
dba_data_files
GROUP BY
tablespace_name
)
a,
(
SELECT
tablespace_name,
sum(bytes) bytes
FROM
dba_free_space
GROUP BY
tablespace_name
)
f
WHERE
d.tablespace_name = a.tablespace_name(+)
AND d.tablespace_name = f.tablespace_name(+)
AND NOT
(
d.extent_management like 'LOCAL'
AND d.contents like 'TEMPORARY'
)
UNION ALL
SELECT
d.tablespace_name TS,
d.status "Status",
d.contents "Type",
d.bigfile "Bigfile",
--d.extent_management "Extent Mgmt",
ROUND(NVL(a.MAXBYTES1 , 0), 3)/1024/1024 "MaxSize MB",
ROUND(NVL(a.bytes, 0), 3)/1024/1024 "Alloz MB",
ROUND(NVL(t.bytes, 0), 3)/1024/1024 "Belegt MB",
ROUND(NVL(t.bytes / a.MAXBYTES1 * 100, 0), 2) "Bel_MS %",
ROUND(NVL(t.bytes / a.bytes * 100, 0), 2) "Bel_AL %"
FROM
sys.dba_tablespaces d,
(
SELECT
tablespace_name,
sum(bytes) bytes,
sum(decode(autoextensible,'YES',maxbytes,bytes)) MAXBYTES1
FROM
dba_temp_files
GROUP BY
tablespace_name
)
a,
(
SELECT
tablespace_name,
sum(bytes_cached) bytes
FROM
v$temp_extent_pool
GROUP BY
tablespace_name
)
t
WHERE
d.tablespace_name = a.tablespace_name(+)
AND d.tablespace_name = t.tablespace_name(+)
AND d.extent_management like 'LOCAL'
AND d.contents like 'TEMPORARY'
order by 1;

prompt
prompt Bel_MS % = 100 / MaxSize * Belegt
prompt Bel Al % = 100 / Alloz * Belegt
prompt
