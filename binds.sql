PROMPT Show Bind Variables for SQL ID &1
col name for a10
col datatype_string for a20
col value_string for a90
select   position,
         name,
         datatype_string,
         was_captured,
         CASE WHEN datatype_string = 'TIMESTAMP' THEN to_char(anydata.accesstimestamp(value_anydata))
           ELSE value_string end as value_string
from     v$sql_bind_capture
where    sql_id = '&1'
order by position;
