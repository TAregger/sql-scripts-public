@@saveset
set timing off

col comments for a200
col column_name for a35
col nullable for a10
col datatype for a22

SELECT comments FROM user_tab_comments tc WHERE tc.table_name = upper('&1');

SELECT TC.COLUMN_NAME,
       DECODE(TC.NULLABLE, 'N', 'NOT NULL', '') AS NULLABLE,
       TC.DATA_TYPE || ' ' ||
       CASE WHEN tc.DATA_TYPE IN ('VARCHAR2', 'CHAR') THEN
          '(' || tc.char_length || ' ' || DECODE(tc.CHAR_USED, 'C', 'CHAR', 'BYTE') || ')'
       ELSE
          NVL2(TC.DATA_PRECISION, '(' || TC.DATA_PRECISION || DECODE(tc.DATA_SCALE, 0, '', ',' || tc.DATA_SCALE) || ')', '')
       END AS datatype,
       cc.comments
  FROM USER_TAB_COLUMNS TC
  JOIN USER_COL_COMMENTS CC ON (CC.table_name = tc.TABLE_NAME AND cc.column_name = tc.COLUMN_NAME)
 WHERE TC.TABLE_NAME = upper('&1')
 ORDER BY TC.COLUMN_ID ASC;


@@loadset

