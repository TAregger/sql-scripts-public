col bind_name for a10
col data for a40

select snap_id, sql_id, name as bind_name, datatype_string,
case
  when substr(datatype_string,1,8) = 'VARCHAR2' THEN VALUE_STRING
  when datatype_string = 'NUMBER' THEN VALUE_STRING
  when datatype_string = 'TIMESTAMP' THEN to_char(anydata.accesstimestamp(value_anydata), 'yyyy-mm-dd hh24:mi:ss')
  ELSE 'UNSUPPORTED DATATYPE'
end as data
from DBA_HIST_SQLBIND
where &1;
