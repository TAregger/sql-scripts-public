prompt ..............
prompt p1: start time &1
prompt p2:   end time &2
prompt ..............
select trunc((to_date('&2', 'Mon dd, yyyy fmhh:mi:ss PM')-to_date('&1', 'Mon dd, yyyy fmhh:mi:ss PM'))*24*60) || ' min' as minutes from dual;
