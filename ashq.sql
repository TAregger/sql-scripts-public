--------------------------------------------------------------------------------
-- Author:      Thomas Aregger
--              
-- Usage:       
--     @ashq <grouping_cols> <filters> <fromtime> <totime>
--
-- Example:
--     @ashq username,sql_id session_type='FOREGROUND' sysdate-1/24 sysdate
--
-- Other:
--     This script uses only the in-memory V$ACTIVE_SESSION_HISTORY
--              
--------------------------------------------------------------------------------
COL totalseconds HEAD "Total|Seconds" FOR 99999999
COL pct HEAD "Pct%" FOR a7
break on report
compute sum of totalseconds on report

set heading off timing off
select 'From: '|| case when to_date(to_char(sysdate, 'dd.mm.yyyy ')||'&3', 'dd.mm.yyyy hh24:mi') >= sysdate then to_date(to_char(sysdate-1, 'dd.mm.yyyy ')||'&3', 'dd.mm.yyyy hh24:mi') else to_date(to_char(sysdate, 'dd.mm.yyyy ')||'&3', 'dd.mm.yyyy hh24:mi') end from dual
union
select 'To:   '|| case when '&4' = '-' then sysdate when to_date(to_char(sysdate, 'dd.mm.yyyy ')||'&4', 'dd.mm.yyyy hh24:mi') >= sysdate then to_date(to_char(sysdate-1, 'dd.mm.yyyy ')||'&4', 'dd.mm.yyyy hh24:mi') else to_date(to_char(sysdate, 'dd.mm.yyyy ')||'&4', 'dd.mm.yyyy hh24:mi') end from dual;
set heading on timing on


select count(*) totalseconds,
    LPAD(ROUND(RATIO_TO_REPORT(COUNT(*)) OVER () * 100)||'%',5,' ') pct,
    &1
from v$active_session_history
where &2
and sample_time
between
(select case when to_date(to_char(sysdate, 'dd.mm.yyyy ')||'&3', 'dd.mm.yyyy hh24:mi') >= sysdate then to_date(to_char(sysdate-1, 'dd.mm.yyyy ')||'&3', 'dd.mm.yyyy hh24:mi') else to_date(to_char(sysdate, 'dd.mm.yyyy ')||'&3', 'dd.mm.yyyy hh24:mi') end from dual)
and
(select case when '&4' = '-' then sysdate when to_date(to_char(sysdate, 'dd.mm.yyyy ')||'&4', 'dd.mm.yyyy hh24:mi') >= sysdate then to_date(to_char(sysdate-1, 'dd.mm.yyyy ')||'&4', 'dd.mm.yyyy hh24:mi') else to_date(to_char(sysdate, 'dd.mm.yyyy ')||'&4', 'dd.mm.yyyy hh24:mi') end from dual)
group by &1 order by 1 desc,&1;

