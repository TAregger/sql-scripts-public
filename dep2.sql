--------------------------------------------------------------------------------
--
-- File name:   dep2.sql
-- Purpose:     Shows dependencies for specified object
--
-- Author:      Thomas Aregger
--              
-- Usage:       @dep2 [schema.]<object_name_pattern>
--              @dep2 mytable
--              @dep2 system.table
--              @dep2 sys%.%tab%
--
--------------------------------------------------------------------------------


col object for a90
col object_type for a30
col "PRIVILEGES" for a40
col "COLUMNS" for a90

PROMPT #
PROMPT # Dependencies for Object &1
PROMPT #

SELECT OWNER||'.'||OBJECT_NAME AS object, OBJECT_TYPE
FROM ALL_OBJECTS
WHERE upper(object_name) LIKE upper(
  CASE
    WHEN INSTR('&1','.') > 0
    THEN SUBSTR('&1',INSTR('&1','.')+1)
    ELSE '&1'
  END )
AND owner LIKE
  CASE
    WHEN INSTR('&1','.') > 0
    THEN UPPER(SUBSTR('&1',1,INSTR('&1','.')-1))
    ELSE USER
  END
UNION ALL
  (SELECT RPAD(' ',4*LEVEL)||'|_'||A.OWNER||'.'||A.NAME,
    A.TYPE
  FROM
    ( SELECT * FROM ALL_DEPENDENCIES ) A
    START WITH upper(referenced_name) LIKE upper(
    CASE
      WHEN INSTR('&1','.') > 0
      THEN SUBSTR('&1',INSTR('&1','.')+1)
      ELSE '&1'
    END )
  AND upper(referenced_owner) LIKE
    CASE
      WHEN INSTR('&1','.') > 0
      THEN UPPER(SUBSTR('&1',1,INSTR('&1','.')-1))
      ELSE USER
    END
    CONNECT BY NOCYCLE ((REFERENCED_NAME = PRIOR NAME)
  AND (REFERENCED_OWNER                  = PRIOR OWNER))
  )
/

PROMPT #
PROMPT # FK Constraints to &1
PROMPT #


select table_name, constraint_name, r_constraint_name
from all_constraints
where owner LIKE
  CASE
    WHEN INSTR('&1','.') > 0
    THEN UPPER(SUBSTR('&1',1,INSTR('&1','.')-1))
    ELSE USER
  END
and 
r_constraint_name in (select CONSTRAINT_NAME from all_constraints
WHERE table_name LIKE upper(
  CASE
    WHEN INSTR('&1','.') > 0
    THEN SUBSTR('&1',INSTR('&1','.')+1)
    ELSE '&1'
  END )
and owner LIKE
  CASE
    WHEN INSTR('&1','.') > 0
    THEN UPPER(SUBSTR('&1',1,INSTR('&1','.')-1))
    ELSE USER
  END)
/

PROMPT #
PROMPT # FK Constraints from &1
PROMPT #
select c1.CONSTRAINT_NAME,(select table_name from all_constraints where constraint_name=c1.r_constraint_name and owner=c1.owner) as table_name, c1.R_CONSTRAINT_NAME from all_constraints c1 where c1.CONSTRAINT_TYPE='R'
and c1.table_name LIKE upper(
  CASE
    WHEN INSTR('&1','.') > 0
    THEN SUBSTR('&1',INSTR('&1','.')+1)
    ELSE '&1'
  END )
and c1.owner LIKE
  CASE
    WHEN INSTR('&1','.') > 0
    THEN UPPER(SUBSTR('&1',1,INSTR('&1','.')-1))
    ELSE USER
  END
/


PROMPT #
PROMPT # Indexes on &1
PROMPT #
select index_owner||'.'||index_name as index_name, listagg(column_name, ', ') within group (order by column_position) as "COLUMNS" from all_ind_columns
WHERE table_name LIKE upper(
  CASE
    WHEN INSTR('&1','.') > 0
    THEN SUBSTR('&1',INSTR('&1','.')+1)
    ELSE '&1'
  END )
and table_owner LIKE
  CASE
    WHEN INSTR('&1','.') > 0
    THEN UPPER(SUBSTR('&1',1,INSTR('&1','.')-1))
    ELSE USER
  END
group by index_owner||'.'||index_name
order by 1
/


PROMPT #
PROMPT # Privileges for Object &1
PROMPT #
SELECT GRANTEE,
  table_schema||'.'||TABLE_NAME AS table_name,
  grantable,
  hierarchy,
  LISTAGG(PRIVILEGE, ', ') within GROUP (ORDER BY privilege) AS "PRIVILEGES"
FROM all_tab_privs
WHERE upper(table_name) LIKE upper(
  CASE
    WHEN INSTR('&1','.') > 0
    THEN SUBSTR('&1',INSTR('&1','.')+1)
    ELSE '&1'
  END )
AND table_schema LIKE
  CASE
    WHEN INSTR('&1','.') > 0
    THEN UPPER(SUBSTR('&1',1,INSTR('&1','.')-1))
    ELSE USER
  END
GROUP BY GRANTEE, table_schema||'.'||TABLE_NAME, grantable, hierarchy
/
