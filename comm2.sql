@@saveset

set lines 300
col column_id for 999
col column_name for a32
col data_type for a20
col comments for a200
select comments from dba_tab_comments where owner='&1' and table_name='&2';

select col.column_id,column_name,col.data_type||' ('||col.data_length||')' as data_type, com.comments from all_tab_columns col join all_col_comments com using(owner,column_name, table_name) where owner='&1' and table_name='&2' order by 1;

@@loadset

