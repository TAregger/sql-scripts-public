-- tkart 17.09.2014
column oracle_username format a30
column os_user_name format a15
column object_name format a37
column object_type format a20
SELECT A.SESSION_ID,
       A.ORACLE_USERNAME,
       A.OS_USER_NAME,
       B.OWNER "OBJECT OWNER",
       B.OBJECT_NAME,
       B.OBJECT_TYPE,
       A.LOCKED_MODE
  FROM (SELECT OBJECT_ID,
               SESSION_ID,
               ORACLE_USERNAME,
               OS_USER_NAME,
               LOCKED_MODE || ': ' || decode(locked_mode, 1, 'null (NULL)', 2, 'row-S (SS)', 3, 'row-X (SX)', 4, 'share (S)', 5, 'S/Row-X (SSX)', 6, 'exclusive (X)') as locked_mode
          FROM V$LOCKED_OBJECT) A,
       (SELECT OBJECT_ID, OWNER, OBJECT_NAME, OBJECT_TYPE FROM DBA_OBJECTS) B
 WHERE A.OBJECT_ID = B.OBJECT_ID
   AND B.OWNER LIKE '%&1%'
   AND B.OBJECT_NAME LIKE '%&2%'
/
