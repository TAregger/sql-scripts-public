----------------------------------------------------------------------------------
--
-- Filename:  t.sql
-- Purpose:   Display all tables of specific owner
-- Author:    Thomas Aregger
-- 
----------------------------------------------------------------------------------

set pages 200
select owner,table_name from dba_tables
where owner like upper('%&1%') and table_name like upper('%&2%')
/
