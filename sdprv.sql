rem -------------------------------------------------------------------------
rem        Trivadis AG, Baden/Basel/Bern/Lausanne/Zürem                     Frankfurt/Freiburg i.Br./Mü/Stuttgart
rem                     Switzerland/Germany Internet: http://www.trivadis.com
rem -------------------------------------------------------------------------
rem   $Header$
rem   **********************************************************************
rem   Group/Privileges.: DBA
rem   Script-Name......: sdprv.sql
rem   Developer........: Andri Kisseleff (ank) andri.kisseleff@trivadis.com
rem   Date.............: 21.07.1994
rem   Version..........: Oracle10g
rem   Usage............: Show privileges of any user
rem   Input parameters.:
rem   Output.......... : 
rem   Called by........:
rem   Remarks..........: Before using this script one *must* create a view
rem                      and grant select to DBAs:
rem
REM                CONNECT SYS/pwd
REM                CREATE VIEW X$KZDOS_DBA AS SELECT * FROM X$KZDOS;
REM                GRANT SELECT ON X$KZDOS_DBA TO select_catalog_role;
rem -----------------------------------------------------------------------
rem
rem $Log$
rem Revision 1.3  2005/04/03 10:17:53  far
rem Group by entfernt
rem
rem Revision 1.2  2003/09/09 18:12:58  ank
rem - added roles granted to public (only the roles, not their privs.
rem incl. privs it would be too much...)
rem
rem Revision 1.1  2003/09/09 18:03:35  ank
rem - OK for 10.1
rem
rem Revision 1.3  2002/08/30 13:16:09  far
rem getestet/angepasst fü
rem
rem Revision 1.2  2001/10/30 04:37:50  ank
rem - Work-arround for problem with 0 = SELECT COUNT('x') implemented
rem
rem Revision 1.1  2001/07/23 05:14:27  ank
rem - Initial Release Oracle9i (9.0.x).
rem Does not work with 9.0.0, OK with 9.0.1
rem
rem
rem Changes:
rem DD.MM.YYYY Consultant Change
rem -----------------------------------------------------------------------
rem 02.08.1997 ank        Oracle8
rem 20.04.1999 AnK        OK for Oracle8i
rem 28.08.2002 MaW        OK for Oracle9i R2
rem -----------------------------------------------------------------------
rem
rem
CLEAR BREAKS
COLUMN NAMEVAR NOPRINT NEW_VALUE NAMEVAR
COLUMN DATUM NOPRINT NEW_VALUE DATEVAR
SET PAUSE OFF TIMING OFF VERIFY OFF ECHO OFF TERMOUT ON
ACCEPT grantee PROMPT "Username (no wildcards)...: "
SET TERMOUT OFF 
        SELECT NAME NAMEVAR,
               TO_CHAR(SYSDATE,'DD.MM.YYYY HH24:MI') DATUM
        FROM   SYS.USER$
        WHERE  NAME=UPPER('&grantee')
/
SET TERMOUT ON
SPOOL sdprv.lis
ACCEPT DUMMY prompt '<<<RETURN>>> for all Roles...'
TTITLE LEFT "All roles of the user" -
       RIGHT NAMEVAR SKIP RIGHT DATEVAR
        SELECT  'Direct grant to user...' GRANTED, GRANTED_ROLE, DEFAULT_ROLE, ADMIN_OPTION
        FROM    DBA_ROLE_PRIVS
        WHERE   GRANTEE=upper('&grantee')
         UNION
        SELECT  'Grant to Public........' GRANTED, GRANTED_ROLE, DEFAULT_ROLE, ADMIN_OPTION
        FROM    DBA_ROLE_PRIVS
        WHERE   GRANTEE=upper('PUBLIC')
        ORDER BY 1,2
/
ACCEPT DUMMY prompt '<<<RETURN>>> for Sub-Rolles...'
TTITLE LEFT "Sub-Roles of the User" -
       RIGHT NAMEVAR SKIP RIGHT DATEVAR
        SELECT U1.NAME ROLE,U2.NAME "SUB-ROLE"
        FROM  SYS.USER$ U1, SYS.USER$ U2, SYS.SYSAUTH$ SA
        WHERE GRANTEE# IN
           (SELECT DISTINCT(PRIVILEGE#)
            FROM SYS.SYSAUTH$ SA
            WHERE PRIVILEGE# > 0
            CONNECT BY PRIOR SA.PRIVILEGE# = SA.GRANTEE#
            START WITH GRANTEE#=(SELECT USER#
                                 FROM SYS.USER$
                                 WHERE NAME=UPPER('&grantee'))
                         OR GRANTEE#=1 OR GRANTEE# IN
              (SELECT KZDOSROL FROM SYS.X$KZDOS))
           AND U1.USER#=SA.GRANTEE# AND U2.USER#=SA.PRIVILEGE#
        GROUP BY U1.NAME,U2.NAME
        UNION
        select 
          'No Sub-Roles',''
        FROM  DUAL
        WHERE   to_number(0) = ( SELECT COUNT(*)
        FROM  SYS.USER$ U1, SYS.USER$ U2, SYS.SYSAUTH$ SA
        WHERE GRANTEE# IN
           (SELECT DISTINCT(PRIVILEGE#)
            FROM SYS.SYSAUTH$ SA
            WHERE PRIVILEGE# > 0
            CONNECT BY PRIOR SA.PRIVILEGE# = SA.GRANTEE#
            START WITH GRANTEE#=(SELECT USER#
                                 FROM SYS.USER$
                                 WHERE NAME=UPPER('&grantee'))
                         OR GRANTEE#=1 OR GRANTEE# IN
              (SELECT KZDOSROL FROM SYS.X$KZDOS))
           AND U1.USER#=SA.GRANTEE# AND U2.USER#=SA.PRIVILEGE#)
/
ACCEPT DUMMY prompt '<<<RETURN>>> for System-Privileges...'
SET PAGES 66 PAUSE ON PAUSE <<<RETURN>>>
BREAK ON ROLE SKIP 1
TTITLE LEFT "System-Privileges (roles and directly)" -
       RIGHT NAMEVAR SKIP RIGHT DATEVAR
        SELECT U.NAME ROLE,SPM.NAME PRIVILEGE,
               DECODE(MIN(OPTION$),1,'YES','NO') ADM
        FROM  SYS.USER$ U, SYS.SYSTEM_PRIVILEGE_MAP SPM, SYS.SYSAUTH$ SA
        WHERE GRANTEE# IN
           (SELECT DISTINCT(PRIVILEGE#)
            FROM SYS.SYSAUTH$ SA
            WHERE PRIVILEGE# > 0
            CONNECT BY PRIOR SA.PRIVILEGE# = SA.GRANTEE#
            START WITH GRANTEE#=(SELECT USER#
                                 FROM   SYS.USER$
                                 WHERE  NAME=UPPER('&grantee'))
               OR GRANTEE#=1 OR GRANTEE# IN
              (SELECT KZDOSROL FROM SYS.X$KZDOS))
          AND U.USER#=SA.GRANTEE# AND SA.PRIVILEGE#=SPM.PRIVILEGE
        GROUP BY U.NAME, SPM.NAME
        UNION
        SELECT  'direct' ROLE, PRIVILEGE, ADMIN_OPTION
        FROM    SYS.DBA_SYS_PRIVS
        WHERE   GRANTEE = UPPER('&grantee')
        ORDER BY 1,2
/
ACCEPT DUMMY prompt '<<<RETURN>>> for Object-Privileges...'
TTITLE LEFT "Objekt-Privileges (roles and direclty)" -
       RIGHT NAMEVAR SKIP RIGHT DATEVAR
COLUMN ROLE FORMAT A25 TRUNC
COLUMN OWNER FORMAT A15 TRUNC
COLUMN OBJECT_NAME FORMAT A30 TRUNC
COLUMN PRIVILEGE FORMAT A3 TRUNC
SET PAGES 66
SET PAUSE <<<RETURN>>> PAUSE OFF
        SELECT U1.NAME ROLE,U2.NAME OWNER,O.NAME object_name,
               TPM.NAME PRIVILEGE,
               DECODE(MAX(OA.OPTION$), 1, 'YES', 'NO') GRANTABLE
        FROM  SYS.USER$ U1,SYS.USER$ U2,SYS.TABLE_PRIVILEGE_MAP TPM,
              SYS.OBJAUTH$ OA,SYS.OBJ$ O,SYS.COL$
        WHERE GRANTEE# IN
           (SELECT DISTINCT(PRIVILEGE#)
            FROM SYS.SYSAUTH$ SA
            WHERE PRIVILEGE# > 0
            CONNECT BY PRIOR SA.PRIVILEGE# = SA.GRANTEE#
            START WITH GRANTEE#=(SELECT USER#
                                 FROM   SYS.USER$
                                 WHERE  NAME=UPPER('&grantee'))
                  OR GRANTEE#=1 OR GRANTEE# IN
              (SELECT KZDOSROL FROM SYS.X$KZDOS))
           AND U1.USER#=OA.GRANTEE# AND OA.PRIVILEGE#=TPM.PRIVILEGE
           AND OA.OBJ#=O.OBJ# AND OA.OBJ#=COL$.OBJ#(+) AND OA.COL#=COL$.COL#(+)
           AND U2.USER#=O.OWNER#
           AND COL$.COL# IS NULL
        GROUP BY U1.NAME,U2.NAME,O.NAME,COL$.NAME,TPM.NAME
        UNION
        SELECT  'direct' ROLE, OWNER, TABLE_NAME object_name, 
                PRIVILEGE, GRANTABLE
        FROM    SYS.DBA_TAB_PRIVS
        WHERE   GRANTEE=UPPER('&grantee')
        ORDER BY 1,2,3,4
/
ACCEPT DUMMY prompt '<<<RETURN>>> for Column-Privileges...'
TTITLE LEFT "Column-Privileges (roles and directly)" -
       RIGHT NAMEVAR SKIP RIGHT DATEVAR
COLUMN ROLE FORMAT A16 TRUNC
COLUMN OWNER FORMAT A10 TRUNC
COLUMN OBJECT_NAME FORMAT A20 TRUNC
COLUMN COLUMN_NAME FORMAT A22 TRUNC
COLUMN PRIVILEGE FORMAT A3 TRUNC
SET PAGES 66
SET PAUSE <<<RETURN>>> PAUSE OFF
        SELECT U1.NAME ROLE,U2.NAME OWNER,O.NAME object_name,
               COL$.NAME COLUMN_NAME, TPM.NAME PRIVILEGE,
               DECODE(MAX(OA.OPTION$), 1, 'YES', 'NO') GRANTABLE
        FROM  SYS.USER$ U1,SYS.USER$ U2,SYS.TABLE_PRIVILEGE_MAP TPM,
              SYS.OBJAUTH$ OA,SYS.OBJ$ O,SYS.COL$
        WHERE GRANTEE# IN
           (SELECT DISTINCT(PRIVILEGE#)
            FROM SYS.SYSAUTH$ SA
            WHERE PRIVILEGE# > 0
            CONNECT BY PRIOR SA.PRIVILEGE# = SA.GRANTEE#
            START WITH GRANTEE#=(SELECT USER#
                                 FROM   SYS.USER$
                                 WHERE  NAME=UPPER('&grantee'))
                  OR GRANTEE#=1 OR GRANTEE# IN
              (SELECT KZDOSROL FROM SYS.X$KZDOS))
           AND U1.USER#=OA.GRANTEE# AND OA.PRIVILEGE#=TPM.PRIVILEGE
           AND OA.OBJ#=O.OBJ# AND OA.OBJ#=COL$.OBJ#(+) AND OA.COL#=COL$.COL#(+)
           AND U2.USER#=O.OWNER#
           AND COL$.COL# IS NOT NULL
        GROUP BY U1.NAME,U2.NAME,O.NAME,COL$.NAME,TPM.NAME
        UNION
        SELECT  'direct' ROLE, OWNER, TABLE_NAME object_name, COLUMN_NAME, 
                PRIVILEGE, GRANTABLE
        FROM    SYS.DBA_COL_PRIVS
        WHERE   GRANTEE=UPPER('&grantee')
        ORDER BY 1,2,3,4
/
SPOOL OFF
CLEAR BREAKS 
COL NAMEVAR CLEAR
COL DATUM CLEAR
COL ROLE CLEAR
COL OWNER CLEAR
COL OBJECT_NAME CLEAR
COL COLUMN_NAME CLEAR
COL PRIVILEGE CLEAR
TTITLE OFF
PROMPT
PROMPT ed sdprv.lis for the listing of the privileges...
PROMPT
